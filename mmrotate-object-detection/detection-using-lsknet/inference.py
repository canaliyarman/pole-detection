import mmcv
from mmrotate.apis import init_detector, inference_detector, show_result_pyplot

# Specify the path to model config and checkpoint file
config_file = 'configs\custom\lsknet_custom.py'
checkpoint_file = 'work_dirs\lsknet_custom\latest.pth'

# Initialize the detector
model = init_detector(config_file, checkpoint_file, device='cuda:0')

# Test a single image
img = 'C:/Users/CanAliYarman/Documents/MMrot/ETDII/test/images/5e690e4824a84c85b5a09a8b2fdd0ffc.png'  # or img = mmcv.imread(img), which will only load it once
result = inference_detector(model, img)

# Show the results
show_result_pyplot(model, img, result, score_thr=0.3)